FROM golang:1.15.1 AS builder
WORKDIR /build
COPY . /build
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -v -o gitopsmr

FROM alpine:3.10
COPY --from=builder /build/gitopsmr /bin/gitopsmr