module github.com/nitpum/gitopsmr

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/cenkalti/backoff/v4 v4.1.0 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/tidwall/sjson v1.1.1
	github.com/xanzy/go-gitlab v0.38.1
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
