package main

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tidwall/sjson"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	argPrivateToken = kingpin.Arg("token", "Access token used in API").Required().String()
	argProjID       = kingpin.Arg("proj.id", "Git project id").Required().Int()
	argJSONFile     = kingpin.Arg("path", "File to update").Required().String()
	argKeyVal       = kingpin.Arg("keyvalue", "Data to update by key example: prod.version=1,dev.version=2").Required().String()
	flagUnique      = kingpin.Flag("uid", "Unique id").String()
	flagBranch      = kingpin.Flag("branch", "Branch name to create new branch").String()
	flagLabels      = kingpin.Flag("labels", "MR labels").Default("gitopsmr").String()
	flagRefBranch   = kingpin.Flag("ref.branch", "Reference branch name").Default("main").String()
	flagAuthorName  = kingpin.Flag("author.name", "Git author name").Default("gitops").String()
	flagAuthorEmail = kingpin.Flag("author.email", "Git author name").Default("gitops@nitpum.com").String()
	flagAutoMerge   = kingpin.Flag("merge", "Auto merge mr if possible").Default("true").Enum("true", "false")
	flagHasPipeline = kingpin.Flag("ci", "Set true if has pipeline").Default("true").Enum("true", "false")
	flagVerbose     = kingpin.Flag("verbose", "Verbose").Bool()
)

func main() {
	kingpin.Parse()

	// Log init
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Info().Bool("VERBOSE", *flagVerbose)
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *flagVerbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	git, err := gitlab.NewClient(*argPrivateToken)
	if err != nil {
		panic(err)
	}

	uid := *flagUnique
	if uid == "" {
		uid = randUID()
	}
	log.Info().Msgf("UID: %s", uid)

	log.Info().Str("Ref", *flagRefBranch)

	json := getFile(git, *argProjID, *argJSONFile)
	dataToUpdate := strings.Split(strings.TrimSpace(*argKeyVal), ",")
	updated := setJSONKeyVal(json, dataToUpdate)
	log.Debug().Msg(string(updated))
	branch, err := genBranchName(*argJSONFile, uid)
	if err != nil {
		panic(err)
	}
	createBranch(git, *argProjID, branch)
	pushFile(git, *argProjID, *argJSONFile, updated, branch)
	labels := strings.Split(*flagLabels, ",")
	mr := openMr(git, *argProjID, branch, *flagRefBranch, labels)
	mergeMr(git, *argProjID, mr.IID)
}

func randUID() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func setJSONKeyVal(jsonData []byte, keyValue []string) []byte {
	data := jsonData
	for _, pair := range keyValue {
		splitted := strings.SplitN(pair, "=", 2)
		updated, err := sjson.SetBytes(data, splitted[0], splitted[1])
		if err != nil {
			panic(err)
		}
		data = updated
	}

	return data
}

func getFile(git *gitlab.Client, projID int, filePath string) []byte {
	opt := &gitlab.GetRawFileOptions{
		Ref: flagRefBranch,
	}
	json, _, err := git.RepositoryFiles.GetRawFile(projID, filePath, opt)
	if err != nil {
		panic(err)
	}
	return json
}

func createBranch(git *gitlab.Client, projID int, branch string) {
	opt := &gitlab.CreateBranchOptions{
		Branch: &branch,
		Ref:    flagRefBranch,
	}
	b, _, err := git.Branches.CreateBranch(projID, opt)
	if err != nil {
		panic(err)
	}
	respBody, err := json.Marshal(b)
	if err != nil {
		log.Error().Err(err)
	}
	log.Debug().Msg(string(respBody))
}

func pushFile(git *gitlab.Client, projID int, filePath string, data []byte, branch string) {
	opt := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String(branch),
		AuthorEmail:   flagAuthorEmail,
		AuthorName:    flagAuthorName,
		CommitMessage: gitlab.String(fmt.Sprintf("gitops: Update %s", filePath)),
		Content:       gitlab.String(string(data)),
	}
	info, _, err := git.RepositoryFiles.UpdateFile(projID, filePath, opt)
	if err != nil {
		panic(err)
	}
	json, err := json.Marshal(info)
	if err != nil {
		log.Error().Err(err)
	}
	log.Info().Msg(string(json))
}

func genBranchName(filePath string, uid string) (string, error) {
	if *flagBranch != "" {
		return *flagBranch, nil
	}
	if filePath == "" {
		return "", errors.New("No file path")
	}
	return fmt.Sprintf("gitops/%s", uid), nil
}

func openMr(git *gitlab.Client, projID int, source string, target string, labels []string) *gitlab.MergeRequest {
	log.Info().Msg("Open merge request")
	title := fmt.Sprintf("gitops: %s", source)
	opt := &gitlab.CreateMergeRequestOptions{
		Title:              &title,
		SourceBranch:       &source,
		TargetBranch:       &target,
		Labels:             labels,
		RemoveSourceBranch: gitlab.Bool(true),
	}
	mr, _, err := git.MergeRequests.CreateMergeRequest(projID, opt)
	if err != nil {
		log.Error().Err(err)
		panic(err)
	}

	return mr
}

func mergeMr(git *gitlab.Client, projID int, mrID int) {
	if *flagAutoMerge != "true" {
		return
	}

	waitCi := true
	if *flagHasPipeline == "false" {
		waitCi = false
	}

	acceptBackoff := backoff.WithMaxRetries(backoff.NewConstantBackOff(5*time.Second), 6)
	backoff.Retry(func() error {
		opt := &gitlab.AcceptMergeRequestOptions{
			ShouldRemoveSourceBranch:  gitlab.Bool(true),
			MergeWhenPipelineSucceeds: &waitCi,
		}
		_, _, err := git.MergeRequests.AcceptMergeRequest(projID, mrID, opt)
		if err != nil {
			log.Err(err).Msg("Auto merge fail")
			return err
		}
		return nil
	}, acceptBackoff)

}
